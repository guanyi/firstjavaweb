package controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Guanyi on 11/3/2016.
 */
@WebServlet(name = "HelloWorldServlet", urlPatterns = "/showData",loadOnStartup = 0)
public class HelloWorldServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/plain"); // sets the content type
        response.setCharacterEncoding("UTF-8"); // sets the encoding
        PrintWriter out = response.getWriter();
        out.println("<html><head><title>Hello World!</title></head>");
        out.println("<body><h1>Hello World!</h1></body></html>");
    }
}
